<header id="header">
	<!--wrapper-->
	<div class="nav-wrapper">
		<nav id="navbar" class="navbar navbar-toggleable-md navbar-light">
			<div class="container">
            
			    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#nav-primary" aria-controls="nav-primary" aria-expanded="false" aria-label="Toggle navigation">
			        <span></span>
			        <span></span>
			        <span></span>
			    </button>

               
               <!--logo-->
			    <a class="navbar-brand navbar-sticky-brand d-none" href="index.php"><img src="images/logo_agency.png" alt=""></a>
               
                <!--logo when nav is sticky/fixed at the top-->
			    <a class="navbar-brand navbar-sticky-brand logo-primary" href="index.php"><img src="images/logo_agency2.png" alt=""></a>


			    <div id="nav-primary" class="collapse navbar-collapse">
			        <ul class="nav navbar-nav ml-auto">
			            <li class="nav-item"><a class="nav-link" href="#header" >Inicio</a></li>
			            <li class="nav-item"><a class="nav-link" href="#services" >Servicios</a></li>
			            <li class="nav-item"><a class="nav-link" href="#portfolio" >Portafolio</a></li>
			            <li class="nav-item"><a class="nav-link" href="#staff" >Team</a></li>
			            			            <!-- <li class="nav-item"><a class="nav-link" href="#reviews" >Reviews</a></li> -->
			            			            <li class="nav-item"><a class="nav-link" href="#pricing" >Pricing</a></li>
			            <li class="nav-item"><a class="nav-link" href="#app" >App</a></li>
			            <li class="nav-item"><a class="nav-link" href="#blog" >Blog</a></li>
			            <li class="nav-item"><a class="nav-link" href="#contact" >Contacto</a></li>
			         
			        </ul>
			        			    </div>
			    <!--end #nav-primary-->
			    
			</div>
			<!--end container-->
		</nav>
		<!--end navbar-->
	</div>
	<!--end .nav-wrapper -->
</header>