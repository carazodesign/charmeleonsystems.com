<!DOCTYPE <!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Nosotros | Charmeleon Systems | Nicaragua</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon3.png" sizes="16x16" type="image/png">
	<link rel="stylesheet" href="css/bootstrap.agency.min.css">
	<link rel="stylesheet" href="css/tether.min.css">
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">
	<link rel="stylesheet" href="css/datepicker.min.css">
	<link rel="stylesheet" href="css/magnific-popup.min.css">
	<link rel="stylesheet" href="css/theme.agency.min.css">
	<link rel="stylesheet" href="css/custom-animations.css">
	<link rel="stylesheet" href="css/animate.css">
	<link rel="stylesheet" href="css/font-icons.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<script src="https://use.fontawesome.com/642854747b.js"></script>
</head>
<body>
	<?php include("header.php"); ?>

<?php include("footer.php"); ?>
<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/tether.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/isotope.pkgd.min.js"></script>
	<script type="text/javascript" src="js/imagesloaded.pkgd.min.js"></script>
	<script type="text/javascript" src="js/jquery.sticky-customized.min.js"></script>
	<script type="text/javascript" src="js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="js/js.cookie.min.js"></script>
	<script type="text/javascript" src="js/jquery.appear.min.js"></script>
	<script type="text/javascript" src="js/jquery.countTo.min.js"></script>
	<script type="text/javascript" src="js/datepicker.min.js"></script>
	<script type="text/javascript" src="js/datepicker.en.min.js"></script>
	<script type="text/javascript" src="js/wow.min.js"></script>
	<script type="text/javascript" src="js/jquery.lazy.min.js"></script>
	<script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
	<script type="text/javascript" src="js/jquery.form.min.js"></script>
	<script type="text/javascript" src="js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="js/SmoothScroll.min.js"></script>

	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC-aw1KAbpOg9sLpN7U2roUzk4WbOGakFo"></script>	
	<script type="text/javascript" src="js/init.js"></script>
</body>
</html>


