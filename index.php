<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from demo.amviro.com/boka/template/?theme=agency by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 23 Aug 2018 01:18:30 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon3.png" sizes="16x16" type="image/png">
	<title>Charmeleon Systems | Desarrollo, Plublicidad, Diseño Gráfico | Nicaragua </title>
	<link rel="stylesheet" href="css/bootstrap.agency.min.css">
	<link rel="stylesheet" href="css/tether.min.css">
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">
	<link rel="stylesheet" href="css/datepicker.min.css">
	<link rel="stylesheet" href="css/magnific-popup.min.css">
	<link rel="stylesheet" href="css/theme.agency.min.css">
	<link rel="stylesheet" href="css/custom-animations.css">
	<link rel="stylesheet" href="css/animate.css">
	<link rel="stylesheet" href="css/font-icons.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<script src="https://use.fontawesome.com/642854747b.js"></script>
</head>
<body>

<!--BEGIN LOADER -->
<div id="preloader">
	<div class="loader"></div>
</div>
<!--END LOADER -->

<!--HEADER-->
<?php include("header.php"); ?>
<!--END HEADER-->

<!--SECTION SLIDER-->
<section id="slider">
	<div class="contaner-fluid">
		<div id="slides" class="owl-carousel owl-theme">

			<!--slide1-->
			<div class="slide owl-lazy" data-src="images/slide6.jpg">
				<div class="content">
                    <div class="container">
                       <div class="text-left">
                            <h3 class="display-4 slideLeftIn"><span class="bg-inverse">Somos creativos</span></h3>
                            <h2 class="display-1 slideRightIn">Publicidad en Línea</h2>
                            <p class="slideBottomIn">Hacemos realidad tus ideas, impulsamos tus ventas</p>

                            <p class="btns slideBottomIn2">
                                <a href="#about" class="btn btn-primary mr-2">Conócenos</a>
                                <!-- <a href="#" class="btn btn-secondary">Explore</a> -->
                            </p>
                        </div>
                    </div>
				
				</div>
			</div>
			<!--slide1 ends-->

			<!--slide2-->
			<div class="slide owl-lazy" data-src="images/slide-01.jpg">
				<div class="content">
				    <div class="container">
                       <div class="text-center">
                                                        <h3 class="display-4 slideLeftIn"><span class="bg-primary">Aliado tecnológico</span></h3>
                                                                                    <h2 class="display-1 slideRightIn">Desarrollo a tu medida</h2>
                                                                                    <p class="slideBottomIn">Diseñamos, desarrollamos e implementamos herramientas colaborativas para su negocio</p>
                                                        <div class="btns mx-auto  slideBottomIn2">
                                <p class="d-inline-block mr-2 slideBottomIn2">
                                    <a href="nosotros.php" class="btn btn-primary">Conócenos</a>
                                </p>
                                <!-- <p class="d-inline-block slideBottomIn2">
                                    <a href="#" class="btn btn-secondary">Explore</a>
                                </p> -->
                            </div>
                        </div>
                    </div>

				</div>
			</div>
			<!--slide2 ends-->
		</div>
	</div><!--end container-->
</section>
<!--END SECTION SLIDER-->

<!--SECTION ABOUT-->
<section id="about" class="section">
	<div class="container wow softFadeInUp" data-wow-delay="0.3s">	

		<div class="section-header text-center">
			<!-- <span class="display-5 text">Lorem Ipsum</span> -->
			<h2 class="section-title display-2">Acerca de Nosotros</h2>
			<span class="line"><i class="icon-livejournal"></i></span>
		</div>


		<div class="row">
			<div class="col-lg-6 col-md-12 col-sm-12 mb-5">
					<!-- <h2 class="display-2 mb-2">We are</h2> -->
					<h3 class="display-3 mb-4">CHARMELEON SYSTEMS</h3>
					<p class="mb-4">
						Somos una empresa 100% Nicaraguense, conformada por profesionales en todas las areas de servicio que ofrecemos. Nuestra pricipal meta es ofrecerle servicios de calidad y obtener clientes satisfechos que nos hagan referentes en el area ofertante.
					</p>
					<p class="mb-4">
						Tenemos los factores que pueden hacer de nosotros un aliado tecnológico bastante util para el funcionamiento de su empresa pudiendo establecer una relacion de colaboracion entre nuestra empresa y su negocio.
					</p>
					<!-- <a href="#" class="btn btn-md btn-outline-primary">Read More</a> -->
			</div>
			<div class="col-lg-6 col-md-12 col-sm-12">

				<div class="card card-style-3 mb-4 clearfix">
					<div class="card-icon">
						<i class="icon-search"></i>
					</div>
					<div class="card-block">
					    <h4 class="card-title display-4">Creatividad</h4>
					    <p class="card-text">Tratamos de ofrece la mayor creatividad desde el momento de la percepcion de la idea a transformar en herramienta.</p>
					</div>
				</div>

				<div class="card card-style-3 mb-4 clearfix">
					<div class="card-icon">
						<i class="icon-thumbs-o-up"></i>
					</div>
					<div class="card-block">
					    <h4 class="card-title display-4">Productividad</h4>
					    <p class="card-text">Nuestra linea de trabajo va establecidad en crear productos y dar servicios de alta productividad pero también de alta calidad.</p>
					</div>
				</div>

				<div class="card card-style-3 mb-4 clearfix">
					<div class="card-icon">
						<i class="icon-pie"></i>
					</div>
					<div class="card-block">
					    <h4 class="card-title display-4">Flexiblilidad</h4>
					    <p class="card-text">Tratamos a nuestros clientes de una manera que podamos demostrar que mas que eso pueden tener la confianza de expresarnos sus ideas y de esta manera hacer el esfuerzo posible para colaborarle.</p>
					</div>
				</div>
			</div>
			
		</div><!--end row-->
	</div><!--end container-->
</section>
<!--END SECTION ABOUT-->

<!--SECTION SERVICES-->
<section id="services" class="lazy" data-src="images/services-agency.jpg">
	<div class="section has-bg text-center wow softFadeInUp" data-wow-delay="0.3s">
		<div class="container">
           
            <!-- section header-->
            <div class="section-header text-center">
                                <!-- <span class="display-5 text">Cool things we do</span> -->
                                <h2 class="display-2 section-title">Nuestros Servicios</h2>
                <span class="line"><i class="icon-briefcase"></i></span>
            </div>
            <!-- end section header-->
            
			<div class="row">

                <!--service 1-->
				<div class="col-md-3 block">
					<div class="card">
						<div class="card-block">
							<div class="card-icon">
								<i class="icon-parentheses text-primary"></i>							</div>
							<h4 class="card-title">Desarrollo de Aplicaciones</h4>
							<p class="card-text">
								Contamos con la experiencia para el Diseño, Desarrollo e Implementación de sus aplicaciones IOS y Andoriod.
							</p>
						</div>
					</div>
				</div>
				<!--end service 1-->


                <!--service 2-->
				<div class="col-md-3 block">
					<div class="card">
						<div class="card-block">
							<div class="card-icon">
								<i class="icon-grid-o text-primary"></i>							</div>
							<h4 class="card-title">Diseño Web</h4>
							<p class="card-text">
								El proceso para el desarrolo de su sitio web es algo que en <a href="http://www.charmeleonsystems.com" title="">CHARMELEON SYSTEMS</a> sigue una línea bastante detallada para un producto de calidad y alta vistosidad.
							</p>
						</div>
					</div>
				</div>
				<!--end service 2-->

                <!--service 3-->
				<div class="col-md-3 block">
					<div class="card">
						<div class="card-block">
							<div class="card-icon">
								<i class="icon-flag text-primary"></i>							</div>
							<h4 class="card-title">Marketing Online</h4>
							<p class="card-text">
								Ofrecemos nuestra experiencia para el mercadeo de su producto o servicio estableciendo altos niveles en sus ventas.
							</p>
						</div>
					</div>
				</div>
				<!--end service 3-->
				                <!--service 4-->
				<div class="col-md-3 block">
					<div class="card">
						<div class="card-block">
							<div class="card-icon">
								<i class="icon-edit text-primary"></i>							</div>
							<h4 class="card-title">Diseño Gráfico</h4>
							<p class="card-text">
								Colaboradores muy creativos conforman nuestro equipo de trabajo para brindarle la mejor solucion en cuanto a temas de Diseño Gráfico.
							</p>
						</div>
					</div>
				</div>
				<!--end service 4-->
			</div><!--end row-->
			
		</div><!--end container-->
	</div>
</section>
<!--END SECTION SERVICES-->

<!--BEGIN COUNT-->
<section id="count" class="section counter text-center bg-inverse">

	<div class="container">

		<div class="row">
            

			<!-- count1 -->
			<div class="col-md-3 block wow softFadeInUp" data-wow-delay="0.3s">
				<i class="icon-airplane"></i>
				<h3 class="counts display-2 text-primary" data-speed="1000" data-to="12" data-from="0">0</h3>
				<p>Years of experience</p>
			</div>
			<!-- end count1 -->
			

			<!-- count2 -->
			<div class="col-md-3 block wow softFadeInUp" data-wow-delay="0.5s">
				<i class="icon-happy-smiley"></i>
				<h3 class="counts display-2 text-primary" data-speed="500" data-to="629" data-from="0">0</h3>
				<p>Satisfied Reviews</p>
			</div>
			<!-- end count2-->
			
			
			<!-- count3 -->
			<div class="col-md-3 block wow softFadeInUp" data-wow-delay="0.7s">
				<i class="icon-wand"></i>
				<h3 class="counts display-2 text-primary" data-speed="300" data-to="200" data-from="0">0</h3>
				<p>Expert Staffs</p>
			</div>
			<!-- end count3-->

			<!-- count4 -->
			<div class="col-md-3 block wow softFadeInUp" data-wow-delay="0.9s">
				<i class="icon-bag"></i>
				<h3 class="counts display-2 text-primary" data-speed="10000" data-to="60000" data-from="0">0</h3>
				<p>Soical Followers</p>
			</div>
			<!-- end count4 -->

		</div><!--end row-->
	</div><!-- end container -->

</section>
<!--END COUNT-->

<!-- SECCION "SISTEMAS A LA MEDIDA" -->
<section class="mbr-section mbr-parallax-background pt-5 pb-5" id="sistemas-medidas" style="background-color: rgb(2, 116, 231)">
<div class="container">
	<div class="row">
			<div class="col-md-6 col-12">
			<div class="lead">
				<p class="text-white display-2">Sistemas a la medida</p>
      <p class="hvr-underline-from-left" style="font-size: 20px;"><strong class="colorblack">Desarrollo con estándares y tendencias nuevas...</strong></p>
      <p>Desa <strong class="colorblack">estándares más altos de la industria en seguridad web</strong> y pasan por un proceso que asegura que la experiencia de usuario sea agradable <strong class="colorblack">y se ajuste a las necesidades de la empresa.</strong>
      </p>
    </div>
		</div>
			<div class="col-md-6 col-12">
			<img src="images/home/5.png" alt="" class="img-fluid">
		</div>
	</div>
</div>	
</section>

<!-- <section class="mbr-section mbr-parallax-background" id="" style="background-color: rgb(2, 116, 231); padding-top: 240px; padding-bottom: 240px;">

 <div class="container">

  <div class="row">
   <div class="mbr-table-md-up" id="sistemas-a-la-medida">

    <div class="mbr-table-cell mbr-right-padding-md-up mbr-valign-top col-md-7 image-size" style="width: 50%;">
     <div class="mbr-figure"><img src="images/Home/5.png" alt="Sistemas a la medida" class="img-fluid"></div>
   </div>

   <div class="mbr-table-cell col-md-5 text-xs-center text-md-right content-size" style="color: #fff;">
     <h3 class="mbr-section-title display-2" style="font-size: 75px;">SISTEMAS A<br>LA MEDIDA</h3>

     <div class="lead">
      <p class="hvr-underline-from-left" style="font-size: 20px;"><strong class="colorblack">Estándares altos en seguridad web...</strong></p>
      <p>los sistemas que desarrollamos cuentan con los <strong class="colorblack">estándares más altos de la industria en seguridad web</strong> y pasan por un proceso que asegura que la experiencia de usuario sea agradable <strong class="colorblack">y se ajuste a las necesidades de la empresa.</strong>
      </p>
    </div>

    <div class="mbr-section-btn">
      <a class="btn btn-lg btn-primary" href="index9f8d.html?seccion=contactanos" style="font-size: 16px; background-color: #1d1e23; border-color: #1d1e23; padding-top: 6px; padding-bottom: 6px; border-radius: 1.6em">
       <strong>&emsp;&emsp;Cotizar&emsp;&emsp;</strong></a> 
     </div>
   </div>

 </div>
</div>
</div>


</section> -->
<!--BEGIN PRICING-->
<section id="pricing" class="section monthly">
    <div class="container text-center wow softFadeInUp">

       <!--section header-->
        <div class="section-header">
           <div class="display-3">Pricing</div>
            <p class="lead">We understand you and so we've created flexible packages that best fit your budget and upgrade as your business grow.</p>
        </div>
        <!--section header-->
        
            <div id="prc-toggler" class="mb-4">
                <span>Monthly</span>
                <span>Yearly</span>
            </div>
        

        <div class="row">

            <!-- package 1-->
            <div class="col-12 col-md-4">
                <div class="wrapper block">
                    <div class="display-4">Basic</div>
                    <div class="display-1 monthly-price"><span>$</span>10</div>
                    <div class="display-1 yearly-price"><span>$</span>90</div>
                    <ul class="features">
                        <li><i class="icon-ok"></i>Feature 1</li>
                        <li><i class="icon-ok"></i>Feature 2</li>
                        <li><i class="icon-cancel"></i>Feature 3</li>
                        <li><i class="icon-cancel"></i>Feature 4</li>
                    </ul>
                    <a href="#" class="btn btn-xl btn-primary">Download</a>
                </div>
            </div>
            <!-- end package 1-->

            <!-- package 2-->
            <div class="col-12 col-md-4 ">
                <div class="wrapper block selected">
                    <div class="display-4">Advanced</div>
                    <div class="display-1 monthly-price"><span>$</span>50</div>
                    <div class="display-1 yearly-price"><span>$</span>550</div>
                    <ul class="features">
                        <li><i class="icon-ok"></i>Feature 1</li>
                        <li><i class="icon-ok"></i>Feature 2</li>
                        <li><i class="icon-ok"></i>Feature 3</li>
                        <li><i class="icon-cancel"></i>Feature 4</li>
                    </ul>
                    <a href="#" class="btn btn-xl btn-outline-secondary">Download</a>
                </div>
            </div>
            <!-- end package 2-->

            <!-- package 3-->
            <div class="col-12 col-md-4">
                <div class="wrapper block">
                    <div class="display-4">Business</div>
                    <div class="display-1 monthly-price"><span>$</span>100</div>
                    <div class="display-1 yearly-price"><span>$</span>995</div>
                    <ul class="features">
                        <li><i class="icon-ok"></i>Feature 1</li>
                        <li><i class="icon-ok"></i>Feature 2</li>
                        <li><i class="icon-ok"></i>Feature 3</li>
                        <li><i class="icon-ok"></i>Feature 4</li>
                    </ul>
                    <a href="#" class="btn btn-xl btn-primary">Download</a>
                </div>
            </div>
            <!-- end package 3-->

        </div>
        <!-- end row-->
    </div>
    <!-- end container-->
</section>
<!-- END PACKAGES -->
    
<!--SECTION SUBSCRIBE-->
<section id="subscribe" class="section bg-inverse text-center wow softFadeInUp" data-wow-delay="0.2s">
	<div class="container">
    
	    <h2 class="display-3 mb-3">Get <strong>hot stuffs</strong> in your inbox</h2>
	    
		<form method="post" action="#" class="col-12 offset-0 col-lg-6 offset-lg-3 col-md-8 offset-md-2 col-sm-8 offset-sm-2">
           <div class="row">
                <div class="block form-group col-12 col-lg-8 col-md-8 col-sm-7">
                    <input type="email" name="email"  value="" class="form-control">
                </div>
                <div class="block form-group col-8 offset-2 col-lg-4 offset-lg-0 col-md-4 offset-md-0 col-sm-5 offset-sm-0">
                    <input type="submit" value="Subscribe" class="btn btn-block btn-primary">
                </div>
            </div>
        </form>
	</div><!--end container-->
</section>
<!--END SECTION SUBSCRIBE-->

<!--SECTION CONTACT/APPOINTMENT-->
<section id="contact" class="section">
	<div class="container wow softFadeInUp" data-wow-delay="0.2s">
		<!-- section header -->
		<div class="section-header text-center">
			<span class="display-5 text">Lorem ipsum</span>
			<h2 class="section-title display-2">Contact Us</h2>
			<span class="line"><i class="icon-mail"></i></span>
			
			<p class="lead">Fill the form, submit and lorem ipsum</p>
			
		</div>
		<!-- section header end -->
		
		<div id="response" class="d-none alert">Lorem ipsum</div>
		
		<form id="form" action="http://demo.amviro.com/boka/template/process.php" method="POST">

			<!--row1-->
			<div class="row">
				<div class="col-lg-6 col-md-12">
					
					<!--row2-->
					<div class="row">
						<!--name-->
					    <div class="form-group col-md-6">
					    	<div class="input-group">
					    		<input type="text" class="form-control" name="name" placeholder="Full Name" required>
					    		<span class="input-group-addon">
			                        <span class="icon-user"></span>
			                    </span>
					    	</div>
					    </div>
					    <!--end name-->

					    <!--email-->
					    <div class="form-group col-md-6">
					    	<div class="input-group">
					    		<input type="text" class="form-control" name="email" placeholder="Email Address" required>
					    		<span class="input-group-addon">
			                        <span class="icon-at"></span>
			                    </span>
					    	</div>
					    </div>
					    <!--end email-->

					    <!--phone-->
					    <div class="form-group col-md-6">
					    	<div class="input-group">
					    		<input type="text" class="form-control" name="phone" placeholder="Phone Number">
					    		<span class="input-group-addon">
			                        <span class="icon-mobile"></span>
			                    </span>
					    	</div>
					    </div>
					    <!--end phone-->
                        
                                                <div class="form-group col-md-6">
					    	<div class="input-group">
					    		<input type="text" class="form-control" name="subject" placeholder="Subject" required>
					    		<span class="input-group-addon">
			                        <span class="icon-edit"></span>
			                    </span>
					    	</div>
					    </div>
					    				    </div><!--end row2-->
				</div>
				
				<!-- message -->
				<div class="col-lg-6 col-md-12">
					<div class="form-group">
						<textarea id="message" placeholder="Message..." class="form-control" rows="4" name="message" required></textarea>
					</div>
				</div>
				<!-- end message -->

				<div class="col-md-12 text-center block">
					<input type="submit" id="submit" class="btn btn-primary btn-lg" value="Book Now">
				</div>

			</div><!--end row1-->
		</form>

	</div><!--end container-->
</section>
<!--END SECTION CONTACT/APPOINTMENT-->

	<!--BEGIN FOOTER-->
<?php include("footer.php"); ?>
<!--END FOOTER-->
	<div id="cookie" class="fixed-bottom alert">
		We use cookies to ensure that we give you the best experience on our website. <a href="#">Click here</a> to find out more. <a href="#" id="cookieBtn" class="btn btn-md">I agree</a>
	</div>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/tether.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/isotope.pkgd.min.js"></script>
	<script type="text/javascript" src="js/imagesloaded.pkgd.min.js"></script>
	<script type="text/javascript" src="js/jquery.sticky-customized.min.js"></script>
	<script type="text/javascript" src="js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="js/js.cookie.min.js"></script>
	<script type="text/javascript" src="js/jquery.appear.min.js"></script>
	<script type="text/javascript" src="js/jquery.countTo.min.js"></script>
	<script type="text/javascript" src="js/datepicker.min.js"></script>
	<script type="text/javascript" src="js/datepicker.en.min.js"></script>
	<script type="text/javascript" src="js/wow.min.js"></script>
	<script type="text/javascript" src="js/jquery.lazy.min.js"></script>
	<script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
	<script type="text/javascript" src="js/jquery.form.min.js"></script>
	<script type="text/javascript" src="js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="js/SmoothScroll.min.js"></script>

	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC-aw1KAbpOg9sLpN7U2roUzk4WbOGakFo"></script>	
	<script type="text/javascript" src="js/init.js"></script>

</body>

<!-- Mirrored from demo.amviro.com/boka/template/?theme=agency by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 23 Aug 2018 01:19:36 GMT -->
</html>
