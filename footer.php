<footer id="footer" class="section-narrow bg-inverse">
    <div class="container text-center wow softFadeInUp">

        <p class="">
            <a href="#" id="BackToTop">
                <i class="icon-up"></i>
            </a>
        </p>

        <div class="mt-3 mb-4">
            <div class="display-5 mb-1">Copyright &copy; <spam id="fecha"></spam> | Charmeleon Systems | Nicaragua</div>
            <div class="display-4 mb-1"><i class="icon-phone"></i> (88) 0123 456 789</div>
            <p class="mb-1"><i class="icon-email "></i> address@bokadomain.com</p>
        </div>

        <!--social icons-->
        <div class="social">
            <span class="twitter"><a href="#"><i class="icon-twitter"></i></a></span>
            <span class="facebook"><a href="#"><i class="icon-facebook"></i></a></span>
            <span class="linkedin"><a href="#"><i class="icon-linkedin"></i></a></span>
            <span class="youtube"><a href="#"><i class="icon-youtube"></i></a></span>
        </div>
        <!--end social icons-->
	</div>
	<!--end container-->
</footer>